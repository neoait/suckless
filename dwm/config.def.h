/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 16;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=11" };
static const char dmenufont[]       = "monospace:size=11";
static const char col_gray1[]       = "#111111";
static const char col_gray2[]       = "#222222";
static const char col_gray3[]       = "#444444";
static const char col_gray4[]       = "#777777";
static const char col_gray5[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_red[]         = "#ee2222";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray1, col_gray1 },
	[SchemeSel]  = { col_gray5, col_gray1, col_red  },
};

/* tagging */
static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
 	/* class     instance  title           tags mask isfloating  monitor */
 	{ "qutebrowser", NULL, NULL,           1 << 8,   0,          -1 },
 	{ "obs",     NULL,     NULL,           1 << 4,   0,          -1 },
 	{ "discord", NULL,  "Discord Updater", 1 << 6,   0,          -1 },
 	{ "discord", NULL,     NULL,           1 << 6,   0,          -1 },
 	{ "lightcord", NULL,   NULL,           1 << 6,   0,          -1 },
 	{ NULL,      "ripcord", NULL,          1 << 6,   0,          -1 },
 	{ NULL,	     NULL,     "Discord",      1 << 6,   0,          -1 },
 	{ "Element", NULL,     NULL,           1 << 5,   0,          -1 },
 	{ "Steam",   NULL,     NULL,           1 << 7,   0,          -1 },
 	{ "Steam",   NULL,     "Friends List", 1 << 7,   1,          -1 },
 	{ NULL, "chatterino",  NULL,           0,        1,          -1 },
 	{ NULL,      NULL,     "newsboat",     1 << 4,   0,          -1 },
 	{ NULL,      NULL,     "pulsemixer",   0,        1,          -1 },
 	{ NULL,      NULL,     "ncmpc",        0,        1,          -1 },
 	{ NULL,      NULL,     "termdown",     0,        1,          -1 },
 	{ NULL,      NULL,     "bc",           0,        1,          -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "<><",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "###",      nrowgrid },
};

/* key definitions */
#define A	Mod1Mask
#define H	Mod3Mask
#define M	Mod4Mask
#define S	ShiftMask
#define C	ControlMask

#include <X11/XF86keysym.h>

#define TAGKEYS(KEY,TAG) \
	{ M,     KEY,      view,           {.ui = 1 << TAG} }, \
	{ M|C,   KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ M|S,   KEY,      tag,            {.ui = 1 << TAG} }, \
	{ H,     KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] 	= { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray5, "-sb", col_red, "-sf", col_gray5, NULL };
static const char *dmenulistcmd[] = { "dmenu_run", "-l", "53", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray5, "-sb", col_red, "-sf", col_gray5, NULL };
static const char *termcmd[]  	= { "st", NULL };
static const char *screencmd[]  = { "screentoggle", NULL };
static const char *unhidecmd[]  = { "mpvctl", "unhide", NULL };
static const char *quitcmd[]  	= { "killall", "xinit", NULL };
static const char *redditcmd[]  = { "dmenureddit", NULL };
static const char *tdtogglecmd[] = { "td-toggle", NULL };
static const char *twitchcmd[]  = { "twitch", NULL };
static const char *kboptionscmd[]  = { "kboptions", NULL };
static const char *kblayoutcmd[]  = { "kblayout", NULL };
static const char *passcmd[]  	= { "passmenu", NULL };
static const char *passtypecmd[] = { "passmenu", "--type", NULL };
static const char *mpvpausecmd[]  = { "mpvctl", "pause", NULL };
static const char *mpvplaycmd[]  = { "mpvctl", "play", NULL };
static const char *locatecmd[]  = { "dmenulocate", NULL };
static const char *remapscmd[]  = { "remaps", NULL };
static const char *togglecmd[]  = { "mpc", "toggle", NULL };
static const char *nextcmd[]  	= { "mpc", "next", NULL };
static const char *prevcmd[]  	= { "mpc", "prev", NULL };
static const char *qmenucmd[]  	= { "qmenu_hud", NULL };
static const char *vtogglecmd[]  = { "pulsemixer", "--toggle-mute", NULL };
static const char *volupcmd[] 	= { "pulsemixer", "--change-volume", "+2", NULL };
static const char *voldowncmd[] = { "pulsemixer", "--change-volume", "-2", NULL };
static const char *mondowncmd[] = { "monbrightness", "-65", NULL };
static const char *monupcmd[]   = { "monbrightness", "+65", NULL };

static Key keys[] = {
	/* mod	key	   function     argument */
	{ H,	XK_comma,  spawn,	   {.v = screencmd } },
        { H,    XK_minus,  spawn,          {.v = unhidecmd } },
	{ H,	XK_w,      spawn,	   SHCMD("wmctrl -a $BROWSER || $BROWSER") },
	{ H,	XK_e,      spawn,	   SHCMD("wmctrl -a $FILE || $TERMINAL -n $FILE -e $FILE") },
	{ H,	XK_r,      spawn,	   {.v = redditcmd } },
	{ H,	XK_y,	   spawn,	   SHCMD("wmctrl -a tremc || $TERMINAL -n tremc -e tremc") },
	{ H|S,	XK_y,	   spawn,	   {.v = tdtogglecmd } },
	{ H,	XK_t,      spawn,	   {.v = twitchcmd } },
        { M|S,  XK_o,      spawn,          {.v = kboptionscmd } },
	{ H,	XK_p,      spawn,	   {.v = passcmd } },
	{ H|S,	XK_p,      spawn,	   {.v = passtypecmd } },
        { M,    XK_p,      spawn,          {.v = mpvpausecmd } },
        { M|S,  XK_p,      spawn,          {.v = mpvplaycmd } },
	{ H,	XK_k,      spawn,	   SHCMD("ps -u $USER -o pid,%mem,%cpu,cmd | sort -k2 -r | sed -n '1!p' | dmenu -i -l 15 | awk '{print $1}' | xargs -r kill -15") },
	{ 0,	XK_Print,  spawn,	   SHCMD("maim -s | xclip -selection c -t image/png") },
	{ H,	XK_s,	   spawn,	   SHCMD("pkill -x ncmpc || $TERMINAL -e ncmpc & mpd") },
	{ H|S,	XK_s,	   spawn,	   SHCMD("wmctrl -x -a steam || steam") },
	{ H,	XK_d,	   spawn,	   SHCMD("wmctrl -x -a ripcord || ripcord") },
	{ H,	XK_l,	   spawn,	   SHCMD("wmctrl -a lutris || lutris") },
	{ H,	XK_slash,  spawn,	   {.v = locatecmd } },
        { M,    XK_z,      spawn,          {.v = kblayoutcmd } },
	{ H,	XK_g,	   spawn,	   SHCMD("pkill -x picom || picom --experimental-backends") },
	{ M,	XK_c,	   spawn,	   SHCMD("pkill -x termdown || $TERMINAL -g 55x10 -e termdown -z -Z %R") },
	{ H,	XK_f,	   spawn,	   SHCMD("dmenuhandler $(xclip -o -selection clipboard)") },
	{ H,	XK_v,	   spawn,	   SHCMD("pkill -x pulsemixer || $TERMINAL -e pulsemixer") },
	{ H,	XK_n,      spawn,	   SHCMD("wmctrl -a newsboat || $TERMINAL -n newsboat -e newsboat") },
	{ H|S,	XK_n,      spawn,	   SHCMD("wmctrl -a vimwiki || $TERMINAL -n vimwiki -e $EDITOR -c VimwikiIndex") },
	{ H,	XK_m,      spawn,	   SHCMD("wmctrl -a neomutt || $TERMINAL -n neomutt -e neomutt") },
        { M,    XK_backslash, spawn,       {.v = remapscmd } },
	{ S, 	XF86XK_AudioMute,	  spawn, {.v = togglecmd } },
	{ S, 	XF86XK_AudioRaiseVolume,  spawn, {.v = nextcmd } },
	{ S, 	XF86XK_AudioLowerVolume,  spawn, {.v = prevcmd } },
	{ 0, 	XF86XK_AudioMute,	  spawn, {.v = vtogglecmd } },
	{ 0, 	XF86XK_AudioRaiseVolume,  spawn, {.v = volupcmd } },
	{ 0, 	XF86XK_AudioLowerVolume,  spawn, {.v = voldowncmd } },
	{ 0, 	XF86XK_MonBrightnessDown, spawn, {.v = mondowncmd } },
	{ 0, 	XF86XK_MonBrightnessUp,   spawn, {.v = monupcmd } },

	{ M,    XK_d,      spawn,          {.v = dmenucmd } },
	{ M,    XK_s,      spawn,          {.v = qmenucmd } },
	{ M|S,	XK_BackSpace, spawn,	   {.v = quitcmd } },
	{ M,    XK_semicolon, spawn,       {.v = termcmd } },
	{ M,    XK_b,      togglebar,      {0} },
	{ M,    XK_j,      focusstack,     {.i = +1 } },
	{ M,    XK_k,      focusstack,     {.i = -1 } },
	{ M,    XK_i,      incnmaster,     {.i = +1 } },
	{ M,    XK_o,      incnmaster,     {.i = -1 } },
	{ M,    XK_h,      setmfact,       {.f = -0.05} },
	{ M,    XK_l,      setmfact,       {.f = +0.05} },
	{ M,    XK_space,  zoom,           {0} },
	{ M,    XK_Tab,    view,           {0} },
	{ M,    XK_q,      killclient,     {0} },
	{ M,    XK_t,      setlayout,      {.v = &layouts[0]} },
	{ M,    XK_f,      setlayout,      {.v = &layouts[1]} },
	{ M,    XK_m,      setlayout,      {.v = &layouts[2]} },
	{ M,    XK_g,      setlayout,      {.v = &layouts[3]} },
	{ M,    XK_Return, togglefloating, {0} },
	{ M,    XK_0,      view,           {.ui = ~0 } },
	{ M|S,  XK_0,      tag,            {.ui = ~0 } },
	{ M,    XK_r,      resetnmaster,   {0} },
	{ M,    XK_comma,  focusmon,       {.i = -1 } },
	{ M,    XK_period, focusmon,       {.i = +1 } },
	{ M|S,  XK_comma,  tagmon,         {.i = -1 } },
	{ M|S,  XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(XK_1,                      0)
	TAGKEYS(XK_2,                      1)
	TAGKEYS(XK_3,                      2)
	TAGKEYS(XK_4,                      3)
	TAGKEYS(XK_5,                      4)
	TAGKEYS(XK_6,                      5)
	TAGKEYS(XK_7,                      6)
	TAGKEYS(XK_8,                      7)
	TAGKEYS(XK_9,                      8)
	{ M|S,  XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click             event mask   button       function        argument */
	{ ClkLtSymbol,       0,           Button1,     setlayout,      {0} },
	{ ClkLtSymbol,       0,           Button3,     setlayout,      {.v = &layouts[2]} },
	{ ClkClientWin,      M,           9,           focusmon,       {.i = -1 } },
	{ ClkClientWin,      M,           8,           focusmon,       {.i = +1 } },
	{ ClkRootWin,        M,           9,           focusmon,       {.i = -1 } },
	{ ClkRootWin,        M,           8,           focusmon,       {.i = +1 } },
	{ ClkWinTitle,       0,           Button2,     zoom,           {0} },
	{ ClkWinTitle,       0,           Button4,     focusstack,     {.i = -1 } },
	{ ClkWinTitle,       0,           Button5,     focusstack,     {.i = +1 } },
	{ ClkWinTitle,       M,           Button4,     focusstack,     {.i = -1 } },
	{ ClkWinTitle,       M,           Button5,     focusstack,     {.i = +1 } },
	{ ClkClientWin,      M,           Button4,     focusstack,     {.i = -1 } },
	{ ClkClientWin,      M,           Button5,     focusstack,     {.i = +1 } },
	{ ClkStatusText,     0,           Button2,     spawn,          {.v = termcmd } },
	{ ClkRootWin,        0,           Button2,     spawn,          {.v = termcmd } },
	{ ClkRootWin,        0,           Button3,     spawn,          {.v = dmenulistcmd } },
	{ ClkClientWin,      M,           Button1,     movemouse,      {0} },
	{ ClkClientWin,      M,           Button2,     togglefloating, {0} },
	{ ClkClientWin,      M,           Button3,     resizemouse,    {0} },
 	{ ClkTagBar,         0,           Button1,     view,           {0} },
     	{ ClkTagBar,         0,           Button3,     toggleview,     {0} },
	{ ClkTagBar,         M,           Button1,     tag,            {0} },
	{ ClkTagBar,         M,           Button3,     toggletag,      {0} },
};
