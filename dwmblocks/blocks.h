//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/ /*Command*/				/*Update Interval*/ /*Update Signal*/
	/* {" vol:", "pulsemixer --get-volume | awk '{print $1}'",	" ",	0,	11}, */
	{" ",     "battery",					"% ",	5,	0},
	{" ",     "free --giga | awk '/^Mem/ { print $3\"G/\"$2 }'",	"G ",	5,	0},
	/* {" ",     "brightness",					" ",	0,	12}, */
	{" ",     "date +%R",					" ",	60,	0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
