# My suckless build

This is a repo of my personal suckless builds.
I use dwm almost exclusively, but I do switch between st and alaritty quite frequently.

<img src="busy_desktop.gif" width="600" height="338"/>

## Dwm patches

These are the patches that are applied to dwm

* uselessgap
* pertag
* alwayscenter
* pertag
